	global	rindex
	global	strcmp
	global	strncmp
	global	strcasecmp
	global	strlen
	global	strcspn
	global	strpbrk
	global	strchr
	global	memcpy
	global	memmove
	global	memset
	global	strstr

;;
;; strlen
;;

strlen:

	mov	rax, -1

_strlen_loop:

	inc	rax
	cmp	byte [rdi + rax], 0
	jne	_strlen_loop
	ret

;;
;; strcmp
;;

strcmp:

	mov	rax, -1
	xor	r8, r8
	xor	r9, r9

_strcmp_loop:

	inc	rax
	mov	r8b, [rdi + rax]
	mov	r9b, [rsi + rax]
	cmp	r8b, r9b
	jne	_strcmp_return
	cmp	r8b, 0
	jne	_strcmp_loop

_strcmp_return:

	mov	rax, r8
	sub	rax, r9
	ret

;;
;; rindex
;;

rindex:

	call	strlen
	cmp	sil, 0
	je	_rindex_end

_rindex_loop:

	cmp	rax, 0
	je	_rindex_not_found
	dec	rax
	cmp	byte [rdi + rax], sil
	jne	_rindex_loop

_rindex_end:

	add	rax, rdi
	ret

_rindex_not_found:

	xor	rax, rax
	ret

;;
;; strchr
;;

strchr:

	call	strlen
	cmp	sil, 0
	je	_strchr_end
	mov	rax, -1

_strchr_loop:

	inc	rax
	cmp	byte [rdi + rax], 0
	je	_strchr_not_found
	cmp	byte [rdi + rax], sil
	jne	_strchr_loop

_strchr_end:

	add	rax, rdi
	ret

_strchr_not_found:

	xor	rax, rax
	ret

;;
;; memcpy
;;

memcpy:

	mov	rax, rdi
	xor	r8, r8
	cmp	rdx, 0
	jne	_memcpy_loop
	ret

_memcpy_loop:

	inc	r8
	mov	r9b, [rsi + r8 - 1]
	mov	byte [rdi + r8 - 1], r9b
	cmp	r8, rdx
	jne	_memcpy_loop
	ret

;;
;; memset
;;

memset:

	mov	rax, rdi
	xor	r8, r8
	cmp	rdx, 0
	jne	_memset_loop
	ret

_memset_loop:

	inc	r8
	mov	byte [rdi + r8 - 1], sil
	cmp	r8, rdx
	jne	_memset_loop
	ret

;;
;; strncmp
;;

strncmp:

	mov	rax, -1
	xor	r8, r8
	xor	r9, r9
	cmp	rdx, 0
	jne	_strncmp_loop
	xor	rax, rax
	ret

_strncmp_loop:

	inc	rax
	dec	rdx
	mov	r8b, [rdi + rax]
	mov	r9b, [rsi + rax]
	cmp	r8b, r9b
	jne	_strncmp_return
	cmp	rdx, 0
	je	_strncmp_return
	cmp	r8b, 0
	jne	_strncmp_loop

_strncmp_return:

	mov	rax, r8
	sub	rax, r9
	ret

;;
;; strcasecmp
;;

strcasecmp:

	mov	rax, -1
	xor	r8, r8
	xor	r9, r9

_strcasecmp_loop:

	inc	rax
	mov	r8b, [rdi + rax]
	mov	r9b, [rsi + rax]

	cmp	r8b, 65
	jnge	_strcasecmp_end_first_uncase
	cmp	r8b, 90
	jnle	_strcasecmp_end_first_uncase
	add	r8b, 32

_strcasecmp_end_first_uncase:

	cmp	r9b, 65
	jnge	_strcasecmp_end_uncase
	cmp	r9b, 90
	jnle	_strcasecmp_end_uncase
	add	r9b, 32

_strcasecmp_end_uncase:

	cmp	r8b, r9b
	jne	_strcasecmp_return
	cmp	r8b, 0
	jne	_strcasecmp_loop

_strcasecmp_return:

	mov	rax, r8
	sub	rax, r9
	ret

;;
;; strcspn
;;

strcspn:

	mov	r10, rdi
	mov	r11, rsi
	mov	rdi, rsi
	xor	rsi, rsi
	mov	r9, -1

_strcspn_loop:

	inc	r9
	mov	sil, byte [r10 + r9]
	call	rindex
	cmp	rax, 0
	jne	_strcspn_end
	cmp	byte [r10 + r9], 0
	jne	_strcspn_loop

_strcspn_end:

	mov	rax, r9
	ret

;;
;; strpbrk
;;

strpbrk:

	mov	r10, rdi
	mov	r11, rsi
	mov	rdi, rsi
	xor	rsi, rsi
	mov	r9, -1

_strpbrk_loop:

	inc	r9
	mov	sil, byte [r10 + r9]
	call	rindex
	cmp	rax, 0
	je	_strpbrk_continue
	cmp	sil, 0
	je	_strpbrk_continue
	mov	rax, r10
	add	rax, r9
	ret

_strpbrk_continue:

	cmp	sil, 0
	jne	_strpbrk_loop
	xor	rax, rax
	ret

;;
;; memmove
;;

memmove:

	cmp	rdx, 0
	jne	_memmove_preloop
	mov	rax, rdi
	ret

_memmove_preloop:

	push	rbp
	mov	rbp, rsp
	xor	r8, r8
	sub	rsp, rdx

_memmove_loop:

	inc	r8
	mov	r9, [rsi + r8 - 1]
	mov	[rsp + r8 - 1], r9b
	cmp	r8, rdx
	jne	_memmove_loop
	mov	rsi, rsp
	call	memcpy
	mov	rsp, rbp
	pop	rbp
	ret

;;
;; strstr
;;

strstr:
	mov	r8, rdi
	mov	rdi, rsi
	mov	rsi, r8
	call	strlen
	mov	rdx, rax
	dec	rsi

_strstr_loop:

	inc	rsi
	push	r8
	push	rdx
	call	strncmp
	pop	rdx
	pop	r8
	cmp	al, 0
	je	_strstr_end
	cmp	byte [rsi], 0
	jne	_strstr_loop
	xor	rax, rax
	ret

_strstr_end:

	mov	rax, rsi
	ret
