##
## Makefile for libasm in /home/rigaud_b/rendu/asm_minilibc
## 
## Made by Matthias RIGAUD
## Login   <rigaud_b@epitech.eu>
## 
## Started on  Mon Mar  6 13:06:59 2017 Matthias RIGAUD
## Last update Thu Mar 16 16:39:29 2017 Matthias RIGAUD
##

RM		= \rm -f

CC		= ld

ASM		= nasm -felf64

NAME		= libasm.so

DIR		= sources

SRCS		= $(DIR)/minilibc.asm

OBJS		= $(SRCS:.asm=.o)

SHEETIES	= $(OBJS)

all:            $(NAME)

$(NAME):	$(OBJS)
		@echo -e "\e[0m"
		@$(CC) -o $(NAME) $(OBJS) -shared
		@echo -e "\e[32mAll done ! ==>\e[33m" $(NAME) "\e[32mcreated !\e[0m"

clean:
		@echo -en "\e[0mCleaning .o && .c~ files..."
		@$(RM) $(SHEETIES)
		@echo -e "	 [\e[32mOk !\e[0m]"

fclean:         clean
		@echo -en "\e[39mCleaning executable..."
		@$(RM) $(NAME)
		@echo -e "		 [\e[32mOk !\e[0m]"

re:             fclean all

comp:           re
		@echo -en "\e[0mCleaning .o && .c~ files..."
		@$(RM) $(SHEETIES)
		@echo -e "	 [\e[32mOk !\e[0m]"

%.o:	        %.asm
		@$(ASM) $< && \
		echo -e "\e[32m[OK]" $< "\e[93m"|| \
		echo -e "\e[91;5m[ERR]\e[25m" $< "\e[93m"

.PHONY:         all clean fclean re comp
